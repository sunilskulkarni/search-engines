import math
import sys

import metapy
import pytoml

import os.path
import csv

class InL2Ranker(metapy.index.RankingFunction):
    """
    Create a new ranking function in Python that can be used in MeTA.
    """
    def __init__(self, some_param=1.0):
        self.param = some_param
        # You *must* call the base class constructor here!
        super(InL2Ranker, self).__init__()

    def score_one(self, sd):
        #lda = sd.num_docs / sd.corpus_term_count
        tfn = sd.doc_term_count * math.log2(1.0 + self.param * sd.avg_dl / sd.doc_size)
        tfn_plus_c = tfn + self.param
        cal = math.log2( (sd.num_docs + 1) / (sd.corpus_term_count + 0.5) )
        return sd.query_term_weight * (tfn/tfn_plus_c) * cal


def load_ranker(cfg_file):
    """
    Use this function to return the Ranker object to evaluate, e.g. return InL2Ranker(some_param=1.0) 
    The parameter to this function, cfg_file, is the path to a
    configuration file used to load the index. You can ignore this for MP2.
    """
    #return metapy.index.JelinekMercer()
    return InL2Ranker(1)

def mean_avg_precision(ranker,query,file_name):

    path_bio_urls = os.path.join(os.path.dirname(os.path.realpath(__file__)), file_name)


    with open(path_bio_urls, 'w') as file_bio_urls:
        f_bio_urls = csv.writer(file_bio_urls)

        # Load the query_start from config.toml or default to zero if not found
        with open('config.toml', 'r') as fin:
            cfg_d = pytoml.load(fin)
        query_cfg = cfg_d['query-runner']
        query_start = query_cfg.get('query-id-start', 0)
        # We will loop over the queries file and add each result to the IREval object ev.
        num_results = 10
        with open('cranfield-queries.txt') as query_file:
            for query_num, line in enumerate(query_file):
                query.content(line.strip())
                results = ranker.score(idx, query, num_results)
                avg_p = ev.avg_p(results, query_start + query_num, num_results)
                print("Query {} average precision: {}".format(query_num + 1, avg_p))
                f_bio_urls.writerow(["{}".format(avg_p)])
        ev.map()


if __name__ == '__main__':
    if len(sys.argv) != 2:
        print("Usage: {} config.toml".format(sys.argv[0]))
        sys.exit(1)

    cfg = sys.argv[1]
    print('Building or loading index...')
    idx = metapy.index.make_inverted_index(cfg)
    query = metapy.index.Document()
    InL2_ranker = load_ranker(cfg)
    bm25_ranker = metapy.index.OkapiBM25(k1=1.2, b=0.75, k3=500)
    ev = metapy.index.IREval(cfg)
    mean_avg_precision(InL2_ranker,query,'inl2.avg_p.txt')
    mean_avg_precision(bm25_ranker, query, 'bm25.avg_p.txt')



